package com.bektursun.coronavirustracker.ui.fragment.routesheet

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.bektursun.coronavirustracker.R

class RouteSheetFragment : Fragment() {

    companion object {
        fun newInstance() = RouteSheetFragment()
    }

    private lateinit var viewModel: RouteSheetViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.route_sheet_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(RouteSheetViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
